package de.obdev.util.packing.baler;

import de.obdev.util.packing.baler.checker.Checker;
import de.obdev.util.packing.baler.checker.CheckerBuilder;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {
        Checker checker = new CheckerBuilder().add(BalerTopObject.class).add(BalerTestObject.class)
                .build();

        System.out.println(checker.check(new BalerTopObject()));

    }

}
