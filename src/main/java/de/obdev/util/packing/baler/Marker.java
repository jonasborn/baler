package de.obdev.util.packing.baler;

public enum Marker {
    READ, WRITE, UPDATE, DELETE
}
