package de.obdev.util.packing.baler;

import com.google.common.io.BaseEncoding;
import com.owlike.genson.Genson;
import de.obdev.util.packing.baler.checker.Checker;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Baler {

    static Logger logger = LogManager.getLogger();

    Boolean strict;
    Boolean resolving;

    Checker checker;
    Genson genson;

    Baler(Boolean strict, Boolean resolving, Checker checker, Genson genson) {
        this.strict = strict;
        this.resolving = resolving;
        this.checker = checker;
        this.genson = genson;
    }

    public String pack(Object o) {
        return genson.serialize(o);
    }

    public String packBinary(Object o) {
        byte[] ser = genson.serializeBytes(o);
        return BaseEncoding.base64Url().encode(ser);
    }

    public Object unpackBinary(String base) {
        byte[] ser = BaseEncoding.base64Url().decode(base);
        Object unpacked = genson.deserialize(ser, Object.class);
        checkRunning(unpacked);
        return unpacked;
    }

    public Object unpack(String json) {
        Object unpacked = genson.deserialize(json, Object.class);
        checkRunning(unpacked);
        return unpacked;
    }

    public <T> T unpack(String json, Class<T> type) {
        T unpacked = genson.deserialize(json, type);
        checkRunning(unpacked);
        return unpacked;
    }



    public Boolean check(Object o) throws IllegalAccessException {
        if (logger.getLevel().equals(Level.DEBUG) && checker == null)
            logger.debug("Checking: checker: {}; strict: {}",
                    (checker != null),
                    strict
            );
        if (checker == null && strict)  return false;
        if (checker == null && !strict) return true;
        Checker.Result result = checker.check(o);
        if (logger.getLevel().equals(Level.DEBUG))
            logger.debug("Checking: checker: {}; strict: {}; result: {}",
                    (checker != null),
                    strict,
                    result
            );
        if (strict) {
            if (result == Checker.Result.VALID) return true;
        } else {
            if (result == Checker.Result.VALID || result == Checker.Result.UNHANDLED) return true;
        }
        return false;
    }

    public void checkRunning(Object o) {
        try {
            if (!check(o)) throw new BalerCheckException("Checker refused object, seams as something is missing!");
        } catch (IllegalAccessException e) {
            throw new BalerCheckException("Checker was unable to access class of object");
        }
    }

    public class BalerCheckException extends RuntimeException {
        public BalerCheckException(String message) {
            super(message);
        }

        public BalerCheckException(String message, Throwable cause) {
            super(message, cause);
        }
    }

}
