package de.obdev.util.packing.baler;

import de.obdev.util.packing.baler.checker.Checker;
import de.obdev.util.packing.baler.checker.CheckerBuilder;
import de.obdev.util.packing.baler.checker.CheckerRule;
import de.obdev.util.packing.baler.patcher.JsonPatch;
import de.obdev.util.packing.baler.patcher.Patch;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BalerTestObject {

     String owner;
     String target;
     UUID id = UUID.randomUUID();

    public BalerTestObject() {
    }

    public BalerTestObject(String owner) {
        this.owner = owner;
    }

    public BalerTestObject(String owner, String target) {
        this.owner = owner;
        this.target = target;
    }

    public static void main(String[] args) throws IllegalAccessException, IOException, InstantiationException {

        String target = BalerTestObject.class.getName();
        Map<String, String> definitions = new HashMap<>();
        definitions.put("target", "target");
        definitions.put("owner", "owner");

        CheckerRule rule1 = new CheckerRule(target, new HashMap<>(definitions));
        definitions.put("owner", "target");
        CheckerRule rule2 = new CheckerRule(target, definitions);

        String patch = JsonPatch.create(rule1.getTarget(), rule1, rule2, Patch.class);

        Checker checker = new CheckerBuilder()
                .add(BalerTestObject.class)
                .add(patch)
                .build();

        Baler baler = new BalerBuilder().setStrict(true)
                .setChecker(
                        checker
                )
                .build();
        String o = baler.pack(new BalerTestObject("ha", ""));
        System.out.println(o);
        System.out.println(baler.unpack(o, BalerTestObject.class));
        checker.store(new File("test.json"));

        CheckerBuilder builder = new CheckerBuilder();
        builder.add(new File("test.json")).build();

    }


    @Override
    public String toString() {
        return "BalerTestObject{" +
                "owner='" + owner + '\'' +
                ", target='" + target + '\'' +
                ", id=" + id +
                '}';
    }
}
