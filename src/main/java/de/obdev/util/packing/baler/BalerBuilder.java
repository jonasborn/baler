package de.obdev.util.packing.baler;

import com.owlike.genson.GensonBuilder;
import com.owlike.genson.reflect.VisibilityFilter;
import de.obdev.util.packing.baler.checker.Checker;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BalerBuilder {

    Checker checker = null;
    Boolean strict = true;
    Boolean resolving = true;
    Boolean permissive = false;

    Map<String, Class> aliases = new HashMap<>();

    public BalerBuilder setChecker(Checker checker) {
        this.checker = checker;
        return this;
    }

    public BalerBuilder setStrict(Boolean strict) {
        this.strict = strict;
        return this;
    }

    public BalerBuilder setResolving(Boolean resolving) {
        this.resolving = resolving;
        return this;
    }

    public BalerBuilder setPermissive(Boolean permissive) {
        this.permissive = permissive;
        return this;
    }

    public BalerBuilder addAlias(String name, Class cls) {
        aliases.put(name, cls);
        return this;
    }

    public BalerBuilder addAliases(String base, Class top, Namer namer) {
        Set<Class<?>> classes = new Reflections(base).getSubTypesOf(top);
        classes.forEach(c -> addAlias(namer.name(c), c));
        return this;
    }

    @FunctionalInterface
    public static interface Namer {
        public String name(Class cls);
    }


    public Baler build() {
        GensonBuilder builder = new GensonBuilder()
                .failOnMissingProperty(strict)
                .useRuntimeType(true)
                .useClassMetadata(resolving)
                .useConstructorWithArguments(true)
                .usePermissiveParsing(permissive)
                .setSkipNull(true)
                .useFields(true, VisibilityFilter.ALL);
        aliases.forEach(builder::addAlias);
        return new Baler(strict, resolving, checker, builder.create());
    }

}
