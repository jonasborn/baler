package de.obdev.util.packing.baler.patcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Patch {

    String target;
    List<Map<String, Object>> data = new ArrayList<>();

    public Patch() {
    }

    public Patch(String target, List<Map<String, Object>> data) {
        this.target = target;
        this.data = data;
    }

    public Patch setTarget(String target) {
        this.target = target;
        return this;
    }

    public Patch setData(List<Map<String, Object>> data) {
        this.data = data;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public List<Map<String, Object>> getData() {
        return data;
    }


}
