package de.obdev.util.packing.baler.patcher;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;
import com.owlike.genson.GenericType;
import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class JsonPatch {

    static Genson genson;

    static {
        genson = new GensonBuilder()
                .failOnMissingProperty(false)
                .useRuntimeType(true)
                .useClassMetadata(true)
                .useConstructorWithArguments(true)
                .setSkipNull(true)
                .create();
    }

    public static String create(String oldOne, String newOne) throws IOException {
        JsonNode oldNode = toNode(oldOne);
        JsonNode newNode = toNode(newOne);
        JsonNode result = JsonDiff.asJson(oldNode, newNode);
        return result.toString();
    }

    public static String create(String target, Object oldOne, Object newOne, Class<? extends Patch> ruleType) throws IOException, IllegalAccessException, InstantiationException {
        return create(target, genson.serialize(oldOne), genson.serialize(newOne), ruleType);
    }

    public static String create(String target, String oldOne, String newOne, Class<? extends Patch> ruleType) throws IOException, IllegalAccessException, InstantiationException {
        String result = create(oldOne, newOne);
        Patch patch = ruleType.newInstance();
        patch.setTarget(target);
        GenericType<List<Map<String, Object>>> genericType = new GenericType<List<Map<String, Object>>>() {
        };
        patch.setData(genson.deserialize(result, genericType));

        return genson.serialize(patch);
    }


    public static <T> T apply(Patch patch, T object) throws IOException {
        JsonNode patchNode = toNode(genson.serialize(patch.getData()));
        JsonNode targetNode = toNode(genson.serialize(object));
        return genson.deserialize(
                com.flipkart.zjsonpatch.JsonPatch.apply(
                        patchNode,
                        targetNode
                ).toString(),
                (Class<T>) object.getClass()
        );
    }


    public static <T> T apply(String patch, T object) throws IOException {
        try {
            Patch patchRule = genson.deserialize(patch, Patch.class);
            return apply(patchRule, object);
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        JsonNode patchNode = toNode(patch);
        JsonNode targetNode = toNode(genson.serialize(object));
        return genson.deserialize(
                com.flipkart.zjsonpatch.JsonPatch.apply(
                        patchNode,
                        targetNode
                ).toString(),
                (Class<T>) object.getClass()
        );
    }

    private static JsonNode toNode(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readTree(json);
    }
}
