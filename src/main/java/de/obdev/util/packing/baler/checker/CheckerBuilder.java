package de.obdev.util.packing.baler.checker;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import de.obdev.util.packing.baler.patcher.JsonPatch;
import de.obdev.util.packing.baler.patcher.Patch;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.*;

public class CheckerBuilder {

    Logger logger = LogManager.getLogger();
    static Genson genson;

    static {
        genson = new GensonBuilder()
                .failOnMissingProperty(false)
                .useRuntimeType(true)
                .useClassMetadata(true)
                .useConstructorWithArguments(true)
                .setSkipNull(true)
                .create();
    }

    Boolean status = true;

    Boolean strict = true;

    Map<String, CheckerRule> loadedRules = new HashMap<>();
    Map<String, Patch> loadedPatches = new HashMap<>();

    public static CheckerRule generateStrict(Class<?> cls) {
        Map<String, String> definitions = new HashMap<>();
        getFields(cls).forEach(e -> definitions.put(e.getName(), e.getName()));
        return new CheckerRule(cls.getName(), definitions);
    }

    public static List<CheckerRule> generateRecursive(String area, Class cls) {
        Set<Class<?>> classes = new Reflections(area).getSubTypesOf(cls);
        List<CheckerRule> rules = new ArrayList<>();
        for (Class found : classes) {
            rules.add(generateStrict(found));
        }
        return rules;
    }

    public static List<Field> getFields(final Class cls) {
        final List<Field> fields = new ArrayList<>();
        List<Class> classes = getSuperClasses(cls);
        classes.add(cls);
        for (Class c : classes) {
            if (c != null) for (Field f : c.getDeclaredFields()) fields.add(f);

        }
        return fields;
    }

    public static List<Class> getSuperClasses(Class cls) {
        List<Class> classList = new ArrayList<Class>();
        Class superclass = cls.getSuperclass();
        classList.add(superclass);
        while (superclass != null) {
            cls = superclass;
            superclass = cls.getSuperclass();
            classList.add(superclass);
        }
        return classList;
    }

    public Checker build() {
        if (strict && !status) throw new CheckerBuildException("Unable to add required files on build progress");

        Map<String, ApplicableCheckerRule> buildRules = new HashMap<>();

        logger.info("Building rules ({})", loadedRules.size());
        for (Map.Entry<String, CheckerRule> entry : loadedRules.entrySet()) {
            Patch patch = loadedPatches.get(entry.getKey());
            if (patch != null) {
                try {
                    CheckerRule newRule = JsonPatch.apply(patch, entry.getValue());
                    logger.debug("Patched {}", entry.getValue().getTarget());
                    buildRules.put(entry.getKey(), newRule.build());
                    logger.debug("Build and added rule for {}", entry.getValue().getTarget());
                } catch (IOException e) {
                    e.printStackTrace();
                    logger.warn("Unable to apply patch ({}) on {}: {}",
                            patch, entry.getValue(), e);
                }
            } else {
                buildRules.put(entry.getKey(), entry.getValue().build());
                if (logger.getLevel().equals(Level.DEBUG))
                    logger.debug("Build and added rule for {}", entry.getValue().getTarget());
            }
        }
        logger.info("Checker build {} of {} rules", buildRules.size(), loadedRules.size());
        return new Checker(loadedRules, loadedPatches, buildRules);

    }

    private void manageStatus(Boolean status) {
        if (!status) {
            this.status = false;
        }
    }

    public CheckerBuilder add(String s) {
        manageStatus(
                this.append(s)
        );
        return this;
    }

    public CheckerBuilder add(CheckerRule rule) {
        manageStatus(
                this.append(rule)
        );
        return this;
    }

    public CheckerBuilder add(Patch patchRule) {
        manageStatus(
                this.append(patchRule)
        );
        return this;
    }

    public CheckerBuilder add(List list) {
        manageStatus(
                this.append(list)
        );
        return this;
    }

    public CheckerBuilder add(File file) {
        Boolean status;
        try {
            status = this.append(file);
        } catch (IOException e) {
            status = false;
        }
        manageStatus(status);
        return this;
    }

    public CheckerBuilder add(Class<?> cls) {
        manageStatus(
                this.append(cls)
        );
        return this;
    }

    public Boolean getStatus() {
        return status;
    }

    public CheckerBuilder setStrict(Boolean status) {
        this.strict = status;
        return this;
    }

    private Boolean append(Class<?> cls) {
        return this.append(generateStrict(cls));
    }

    private Boolean append(CheckerRule rule) {
        this.loadedRules.put(rule.getTarget(), rule);
        return true;
    }

    private Boolean append(Patch patch) {
        this.loadedPatches.put(patch.getTarget(), patch);
        return true;
    }

    private Boolean append(List objects) {
        Integer counter = 0;
        for (Object o : objects) {
            if (o instanceof CheckerRule) {
                if (this.append((CheckerRule) o)) counter++;
            } else if (o instanceof Patch) {
                if (this.append((Patch) o)) counter++;
            }
        }
        return objects.size() == counter;
    }

    public CheckerBuilder addRecrusive(String area, Class<?> cls) {
        List<CheckerRule> rules = generateRecursive(area, cls);
        manageStatus(this.append(rules));
        return this;
    }

    private Boolean append(String s) {
        Object unpacked = genson.deserialize(s, Object.class);
        if (unpacked instanceof CheckerRule) {
            return this.append((CheckerRule) unpacked);
        } else if (unpacked instanceof Patch) {
            return this.append((Patch) unpacked);
        } else if (unpacked instanceof List) {
            return this.append((List) unpacked);
        }
        return false;
    }

    private Boolean append(File file) throws IOException {
        Integer counter = 0;
        if (file.isDirectory()) {
            List<File> files = listFiles(new ArrayList<>(), file);
            for (File f : files) {
                byte[] bytes = Files.readAllBytes(f.toPath());
                append(new String(bytes, "UTF-8"));
                counter++;
            }
            return counter == files.size();
        } else {
            byte[] bytes = Files.readAllBytes(file.toPath());
            return append(new String(bytes, "UTF-8"));
        }
    }

    private List<File> listFiles(List<File> files, File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] currentFiles = file.listFiles();
                if (currentFiles != null) {
                    for (File f : files) {
                        listFiles(files, f);
                    }
                }
            } else {
                files.add(file);
            }
        }
        return files;
    }

    private boolean isPrimitive(Class cls) {
        if (java.lang.String.class.isAssignableFrom(cls)) {
            return true;
        } else if (java.lang.Integer.class.isAssignableFrom(cls)) {
            return true;
        } else if (java.lang.Double.class.isAssignableFrom(cls)) {
            return true;
        } else if (java.lang.Long.class.isAssignableFrom(cls)) {
            return true;
        } else if (java.lang.Boolean.class.isAssignableFrom(cls)) {
            return true;
        } else if (java.lang.Character.class.isAssignableFrom(cls)) {
            return true;
        } else if (java.lang.Integer.class.isAssignableFrom(cls)) {
            return true;
        } else if (java.lang.Float.class.isAssignableFrom(cls)) {
            return true;
        } else if (java.lang.Short.class.isAssignableFrom(cls)) {
            return true;
        }
        return false;
    }

    private List<Object> listObjects(List<Object> list, Object o) throws IllegalAccessException {

        if (o instanceof Iterable) {
            listObjects(list, (Iterable) o);
            return list;
        }
        list.add(o);
        for (Field f : getFields(o.getClass())) {
            f.setAccessible(true);
            if (Iterable.class.isAssignableFrom(f.getType())) {
                listObjects(list, (Iterable) f.get(o));
            } else if (!isPrimitive(f.getType())) {
                list.add(listObjects(list, f.get(o)));
            }
        }
        return list;
    }

    private void listObjects(List<Object> list, Iterable it) throws IllegalAccessException {
        for (Object anIt : it) {
            listObjects(list, anIt);
        }
    }

    static class CheckerBuildException extends RuntimeException {
        public CheckerBuildException(String message) {
            super(message);
        }

        public CheckerBuildException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
