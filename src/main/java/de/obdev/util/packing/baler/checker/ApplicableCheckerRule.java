package de.obdev.util.packing.baler.checker;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ApplicableCheckerRule {

    static Logger logger = LogManager.getLogger();
    String target;
    Map<String, String> definitions;
    List<String> required = new ArrayList<>();

    ApplicableCheckerRule(String target, Map<String, String> definitions) {
        this.target = target;
        this.definitions = definitions;
        definitions.forEach((k, v) -> {
            if (!required.contains(v)) required.add(v);
        });
    }


    public Boolean apply(Object o) throws IllegalAccessException {

        List<String> existent = new ArrayList<>();
        for (Field f : Checker.getFields(o.getClass())) {
            f.setAccessible(true);
            if (f.get(o) != null) {
                String res = definitions.get(f.getName());
                if (res != null)
                    existent.add(res);
                else
                    existent.add(f.getName());
            }
        }
        Boolean status = existent.containsAll(required);
        if (logger.getLevel().equals(Level.DEBUG))
            logger.debug("Applied rule (status: {}) on {}, required {}, found {}",
                    status,
                    o.getClass().getName(),
                    required,
                    existent
            );
        return status;
    }

    public Boolean apply(List<String> fields) {
        return (fields.containsAll(required));
    }

    public String getTarget() {
        return target;
    }

    public Map<String, String> getDefinitions() {
        return definitions;
    }
}