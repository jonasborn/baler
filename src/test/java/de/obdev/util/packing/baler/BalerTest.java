package de.obdev.util.packing.baler;

import de.obdev.util.packing.baler.checker.Checker;
import de.obdev.util.packing.baler.checker.CheckerBuilder;
import de.obdev.util.packing.baler.checker.CheckerRule;
import de.obdev.util.packing.baler.patcher.JsonPatch;
import de.obdev.util.packing.baler.patcher.Patch;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.fail;
@FixMethodOrder(MethodSorters.JVM)
public class BalerTest {


    @Test
    public void pack() {
        try {
            Baler baler = new BalerBuilder().build();
            baler.pack(new BalerTestObject());
            baler.pack(new BalerTestObject("tom"));
            baler.pack(new BalerTestObject("tom", "mark"));
        } catch (Exception e) {
            fail("Unable to serialize test objects");
        }
    }

    @Test
    public void packBinary() {
        Baler baler = new BalerBuilder().build();
        baler.packBinary(new BalerTestObject("tom", "mark"));
    }

    @Test
    public void unpackBinary() {
        Baler baler = new BalerBuilder().setStrict(false).build();
        String binary = baler.packBinary(new BalerTestObject("tom", "mark"));
        Object o = baler.unpackBinary(binary);
        if (!(o instanceof BalerTestObject)) fail("Unable to unpack binary");
    }

    @Test
    public void unpackStrict() {
        Baler baler = new BalerBuilder().build();
        String empty = baler.pack(new BalerTestObject());
        String halfEmpty = baler.pack(new BalerTestObject("tom"));
        String full = baler.pack(new BalerTestObject("tom", "mark"));
        try {
            baler.unpack(empty);
            fail("Baler unpacked empty");
        } catch (Exception ignored) {

        }
        try {
            baler.unpack(halfEmpty);
            fail("Baler unpacked half empty");
        } catch (Exception ignored) {

        }
        try {
            baler.unpack(full);
            fail("Baler unpacked full");
        } catch (Exception ignored) {

        }
    }

    @Test
    public void unpackNotStrict() {
        Baler baler = new BalerBuilder().setStrict(false).build();
        String empty = baler.pack(new BalerTestObject());
        String halfEmpty = baler.pack(new BalerTestObject("tom"));
        String full = baler.pack(new BalerTestObject("tom", "mark"));
        BalerTestObject bto;
        bto = baler.unpack(empty, BalerTestObject.class);
        if (bto == null) fail("Unable to unpack empty");
        bto = baler.unpack(halfEmpty, BalerTestObject.class);
        if (bto == null) fail("Unable to unpack half empty");
        bto = baler.unpack(full, BalerTestObject.class);
        if (bto == null) fail("Unable to unpack full");
    }

    @Test
    public void unpackResolving() {
        Baler baler = new BalerBuilder().setStrict(false).build();
        String empty = baler.pack(new BalerTestObject());
        String halfEmpty = baler.pack(new BalerTestObject("tom"));
        String full = baler.pack(new BalerTestObject("tom", "mark"));
        BalerTestObject bto;
        bto = (BalerTestObject) baler.unpack(empty);
        if (bto == null) fail("Unable to unpack empty");
        bto = (BalerTestObject) baler.unpack(halfEmpty);
        if (bto == null) fail("Unable to unpack half empty");
        bto = (BalerTestObject) baler.unpack(full);
        if (bto == null) fail("Unable to unpack full");
    }

    @Test
    public void unpackChecked() {
        Baler baler = new BalerBuilder().setChecker(
                new CheckerBuilder().add(BalerTestObject.class).build()
        ).build();
        String empty = baler.pack(new BalerTestObject());
        String halfEmpty = baler.pack(new BalerTestObject("tom"));
        String full = baler.pack(new BalerTestObject("tom", "mark"));
        try {
            baler.unpack(empty);
            fail("Unpacked empty");
        } catch (Exception e) {

        }
        try {
            baler.unpack(halfEmpty);
            fail("Unpacked half empty");
        } catch (Exception e) {

        }
        baler.unpack(full);
    }

    @Test
    public void unpackCheckedPatched()  {
        String target = BalerTestObject.class.getName();
        Map<String, String> definitions = new HashMap<>();
        definitions.put("target", "target");
        definitions.put("owner", "owner");

        CheckerRule rule1 = new CheckerRule(target, new HashMap<>(definitions));
        definitions.put("owner", "target");
        CheckerRule rule2 = new CheckerRule(target, definitions);

        String patch = null;
        try {
            patch = JsonPatch.create(rule1.getTarget(), rule1, rule2, Patch.class);
        } catch (IOException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
            fail("Unable to create patch");
        }

        Checker checker = new CheckerBuilder()
                .add(BalerTestObject.class)
                .add(patch)
                .build();

        Baler baler = new BalerBuilder().setStrict(true)
                .setChecker(
                        checker
                )
                .build();
        String half = baler.pack(new BalerTestObject("tom"));
        Object result = baler.unpack(half, BalerTestObject.class);
        if (result == null) fail("Unable to unpack half");

    }

    @Test
    public void store() {
        Checker checker = new CheckerBuilder().add(BalerTestObject.class).build();
        try {
            File file = Files.createTempFile("BalerTest", ".json").toFile();
            checker.store(file);
            if (!file.exists()) fail("File not created");
            if (file.exists()) file.delete();
        } catch (IOException e) {
            fail("Unable to create temp file");
        }
    }

    @Test
    public void read() {
        Checker checker = new CheckerBuilder().add(BalerTestObject.class).build();
        try {
            File file = Files.createTempFile("BalerTest", ".json").toFile();
            checker.store(file);
            if (!file.exists()) fail("File not created");

            Boolean status = new CheckerBuilder().add(file).getStatus();
            if (!status) fail("Unable to load file");
            if (file.exists()) file.delete();
        } catch (IOException e) {
            fail("Unable to create temp file");
        }
    }
}