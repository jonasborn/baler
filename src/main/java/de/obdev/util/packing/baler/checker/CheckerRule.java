package de.obdev.util.packing.baler.checker;

import java.util.HashMap;
import java.util.Map;

public class CheckerRule {


    String target;

    Map<String, String> definitions = new HashMap<>();

    public CheckerRule(String target, Map<String, String> definitions) {
        this.target = target;
        this.definitions = definitions;
    }

    public ApplicableCheckerRule build() {
        return new ApplicableCheckerRule(target, definitions);
    }

    public String getTarget() {
        return target;
    }

    public Map<String, String> getDefinitions() {
        return definitions;
    }

    @Override
    public String toString() {
        return "CheckerRule{" +
                "target='" + target + '\'' +
                ", definitions=" + definitions +
                '}';
    }
}

