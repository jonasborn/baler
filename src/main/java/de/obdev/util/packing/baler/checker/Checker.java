package de.obdev.util.packing.baler.checker;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import de.obdev.util.packing.baler.patcher.Patch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.*;

public class Checker {

    public static List<Class<?>> defaultPrimitives = Arrays.asList(
            String.class,
            Short.class,
            Long.class,
            Character.class,
            Float.class,
            Double.class,
            Boolean.class,
            UUID.class,
            Integer.class
    );
    static Logger logger = LogManager.getLogger();

    Genson genson;
    Map<String, CheckerRule> loadedRules = new HashMap<>();
    Map<String, Patch> loadedPatches = new HashMap<>();
    Map<String, ApplicableCheckerRule> buildRules = new HashMap<>();
    List<Class<?>> primitives = defaultPrimitives;
    Boolean classDirect = true;

    public Checker(Map<String, CheckerRule> loadedRules, Map<String, Patch> loadedPatches, Map<String, ApplicableCheckerRule> buildRules) {
        this.loadedRules = loadedRules;
        this.loadedPatches = loadedPatches;
        this.buildRules = buildRules;

        genson = new GensonBuilder()
                .failOnMissingProperty(false)
                .useRuntimeType(true)
                .useClassMetadata(true)
                .useConstructorWithArguments(true)
                .setSkipNull(true)
                .useIndentation(true)
                .create();

    }

    public static List<Field> getFields(final Class cls) {
        final List<Field> fields = new ArrayList<>();
        List<Class> classes = getSuperClasses(cls);
        classes.add(cls);
        for (Class c : classes) {
            if (c != null) for (Field f : c.getDeclaredFields()) fields.add(f);

        }
        return fields;
    }

    public static List<File> listFiles(List<File> files, File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] currentFiles = file.listFiles();
                if (currentFiles != null) {
                    for (File f : files) {
                        listFiles(files, f);
                    }
                }
            } else {
                files.add(file);
            }
        }
        return files;
    }

    public static List<Class> getSuperClasses(Class cls) {
        List<Class> classList = new ArrayList<Class>();
        Class superclass = cls.getSuperclass();
        classList.add(superclass);
        while (superclass != null) {
            cls = superclass;
            superclass = cls.getSuperclass();
            classList.add(superclass);
        }
        return classList;
    }

    public void store(File file) throws IOException {
        if (!file.exists()) file.createNewFile();
        List<Object> objects = new ArrayList<>();
        objects.addAll(loadedPatches.values());
        objects.addAll(loadedRules.values());
        String data = genson.serialize(objects);
        Files.write(file.toPath(), data.getBytes("UTF-8"));
        logger.info("Stored current rules and patches to {}", file.getPath());
    }

    public Result check(Object object) throws IllegalAccessException {
        List<Object> objects = listObjects(new ArrayList<>(), object);
        for (Object o : objects) {
            if (buildRules.containsKey(o.getClass().getName())) {
                ApplicableCheckerRule rule = buildRules.get(o.getClass().getName());
                if (!rule.apply(o)) {
                    return Result.INVALID;
                } else {
                    return Result.VALID;
                }
            }
        }
        return Result.UNHANDLED;
    }

    public boolean isPrimitive(Class cls) {
        if (classDirect) {
            for (Class primitive: primitives) {
                if (cls.equals(primitive)) return true;
            }
        } else {
            for (Class<?> primitive: primitives) {
                if (primitive.isAssignableFrom(cls)) return true;
            }
        }
        return false;
    }

    public List<Object> listObjects(List<Object> list, Object o) throws IllegalAccessException {
        if (o instanceof Iterable) {
            listObjects(list, (Iterable) o);
            return list;
        }
        list.add(o);
        for (Field f : getFields(o.getClass())) {
            f.setAccessible(true);
            if (Iterable.class.isAssignableFrom(f.getType())) {
                listObjects(list, (Iterable) f.get(o));
            } else if (!isPrimitive(f.getType())) {
                list.add(listObjects(list, f.get(o)));
            }
        }
        return list;
    }

    public void listObjects(List<Object> list, Iterable it) throws IllegalAccessException {
        Iterator i = it.iterator();
        while (i.hasNext()) {
            listObjects(list, i.next());
        }
    }

    public static enum Result {
        VALID(1), INVALID(0), UNHANDLED(-1);

        Integer status;

        Result(int i) {
            this.status = i;
        }

        public Integer toInteger() {
            return status;
        }

    }


}
